package aluis.marte;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by aluis on 3/1/18.
 */
@SuppressWarnings({"unchecked"})
public class DemoFlatten {

    /**
     * Check Junit Test
     */
    public static void main(String[] args) {
        Object[] demoArray = {new Object[]{1,new Object[]{}, new Object[]{2, 3}}, 4};
        Object[] flat = flatten(demoArray).toArray();
        System.out.println("Result: " + Arrays.toString(flat));
    }

    /**
     * Create lineal stream of objects
     *
     * @param array Multi array to check
     * @return Lineal stream
     */
    public static <T> Stream<T> flatten(T[] array) {
        return Arrays.stream(array).flatMap(o -> o.getClass().isArray() ? flatten((T[]) o) : Stream.of(o));
    }
}
