package aluis.marte;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by aluis on 3/1/18.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DemoFlattenTest {

    @Test
    public void simpleTest() {
        Object[] array = {new Object[]{1, 2, new Object[]{3}}, 4};
        assertEquals("[1, 2, 3, 4]", Arrays.toString(DemoFlatten.flatten(array).toArray()));
    }

    @Test
    public void mediumTest() {
        Object[] array = {1, new Object[]{2, new Object[]{3, 4}, new Object[]{5}}, 6};
        assertEquals("[1, 2, 3, 4, 5, 6]", Arrays.toString(DemoFlatten.flatten(array).toArray()));
    }

    @Test
    public void hardTest() {
        Object[] array = {1, 2, new Object[]{3, 4, new Object[]{5}, 6, 7}, 8, 9, 10};
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]", Arrays.toString(DemoFlatten.flatten(array).toArray()));
    }

    @Test
    public void emptyListTest() {
        Object[] array = {1, new Object[]{}, 2, new Object[]{3, 4, new Object[]{5}, 6, 7}, 8, new Object[]{new Object[]{}}, 9, new Object[]{}, 10};
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]", Arrays.toString(DemoFlatten.flatten(array).toArray()));
    }
}
